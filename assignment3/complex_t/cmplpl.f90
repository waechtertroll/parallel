module Complex_Polar
  implicit none
  integer,parameter :: rk=selected_real_kind(18)

  type Complex_t
     real(kind=rk)   :: r, phi
  end type Complex_t

  interface operator(+)
     module procedure add
  end interface operator(+)

  interface operator(-)
     module procedure sub
  end interface operator(-)

  interface operator(*)
     module procedure mul
  end interface operator(*)

  interface operator(/)
     module procedure div
  end interface operator(/)
  
contains

  subroutine setCmpl( x, r, i )
    type(Complex_t)            :: x
    real(kind=rk),intent(in)   :: r, i
    x%r = sqrt( r*r + i*i )
    x%phi = atan2( i, r )
  end subroutine setCmpl

  real function re( x )
    type(Complex_t), intent(in) :: x
    re = x%r * cos( x%phi )
  end function re

  real function im( x )
    type(Complex_t), intent(in) :: x
    im = x%r * sin( x%phi )
  end function im

  real function absolute( x ) ! abs() => shadowing intrinsic abs()
    type(Complex_t), intent(in) :: x
    absolute = x%r
  end function absolute

  function add( a, b ) result( c )
    type(Complex_t), intent(in)  :: a, b
    type(Complex_t)              :: c
    real(kind=rk)                :: i, r

    r = re(a) + re(b)
    i = im(a) + im(b)
    c%r = sqrt( r*r + i*i )
    c%phi = atan2( i, r )
  end function add

  function sub( a, b ) result( c )
    type(Complex_t), intent(in)  :: a, b
    type(Complex_t)              :: c
    real(kind=rk)                :: r, i
    
    r = re(a) - re(b)
    i = im(a) - im(b)
    c%r = sqrt( r*r + i*i )
    c%phi = atan2( i, r )
  end function sub

  function mul( a, b ) result( c )
    type(Complex_t), intent(in)  :: a, b
    type(Complex_t)              :: c

    c%r = a%r * b%r
    c%phi = a%phi + b%phi
  end function mul

  function div( a, b ) result( c )
    type(Complex_t), intent(in)  :: a, b
    type(Complex_t)              :: c

    c%r = a%r - b%r
    c%phi = a%phi - b%phi
  end function div

end module Complex_Polar
     
