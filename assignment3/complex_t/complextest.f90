program main
  use Complex_Polar
  implicit none

  type(Complex_t)     :: a, b
  complex             :: c, d

  c = ( 1.0, 1.0 )
  call setCmpl(a, 1.0_rk, 1.0_rk)
  d = ( -1.0, 1.0 )
  call setCmpl(b, -1.0_rk, 1.0_rk)
  
  print *, "Fortran complex"
  print *, c+d, c-d, c*d, c/d
  print *, "My Complex_t"
  print *, re(a+b), im(a+b), re(a-b),im(a-b), re(a*b), im(a*b), re(a/b), im(a/b)
end program main
