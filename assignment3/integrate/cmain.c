#include <stdio.h>
#include <math.h>

extern double integrate( double(*pFunc)(double), double, double, int );

double func( double x ) {
  return sin(x)*sin(x);
}

int main() {
  printf( "Integration result from Fortran: %f\n",  integrate( func, 0, 1, 150 ) );
  return 0;
}
