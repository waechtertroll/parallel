module MyIntegration
  use iso_c_binding
  implicit none

contains

  function integrate( func, a, b, nsteps ) bind( c )
    real(c_double), intent(in),value :: a, b
    integer(c_int), value            :: nsteps ! no optional in C
    integer                          :: i
    real(c_double)                   :: step, integrate

    interface
       function func( x ) bind( c )
         use iso_c_binding          ! Has to be here... it's not a bug, it's feature
         real(c_double), intent(in) :: x
         real(c_double)             :: func
       end function func
    end interface

    step = (b-a)/nsteps
    integrate = 0; 
    do i=0, nsteps
       integrate = integrate + func( a + i*step ) * step
    end do

  end function integrate

end module MyIntegration
    

     
