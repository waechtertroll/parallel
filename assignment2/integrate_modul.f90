module MyIntegration
  implicit none

contains

  real function integrate( func, a, b, nsteps )
    real, intent(in)      :: a, b
    integer, optional     :: nsteps

    interface
       real function func( x )
         real, intent(in) :: x
       end function func
    end interface

    integer :: i
    real    :: step

    integrate = 0; 
    if ( .not. present( nsteps ) ) nsteps = 100
    step = (b-a) / nsteps

    do i=0, nsteps
       integrate = integrate + func( a + i*step ) * step
    end do

  end function integrate

end module MyIntegration
    

     
