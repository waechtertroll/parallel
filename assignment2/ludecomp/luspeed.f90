program main
  use LU
  implicit none
  
  ! dim:     starting dimension of array = dim x dim
  ! max_exp: states how many times dim will be doubled
  ! before running make sure all .csv are removed from directory

  integer                    :: max_exp=8
  integer                    :: dim = 10
  real(kind=rk), allocatable :: A(:,:)
  integer                    :: i, seedsize
  real(kind=rk)              :: start_time, end_time
  integer, allocatable       :: seeds(:)
  integer                    :: fileA=11, fileB=12, fileC=13

  call random_seed( SIZE=seedsize )
  allocate( seeds( seedsize ) )
  call random_seed( GET=seeds )

  open( unit=fileA, file="procedural.csv", status="new" )
  open( unit=fileB, file="parallel_1.csv", status="new" )
  open( unit=fileC, file="parallel_2.csv", status="new" )

  do i=1, max_exp
     allocate( A(dim,dim) )
     print *, "decomposing matrix of ", dim*dim

     call random_seed( PUT=seeds )
     call random_number( A )
     call cpu_time( start_time )
     call LU_inplace( A )
     call cpu_time( end_time )
     write (fileA, *), dim*dim, end_time - start_time     

     call random_seed( PUT=seeds )
     call random_number( A )
     call cpu_time( start_time )
     call LU_inplace_forall( A )
     call cpu_time( end_time )
     write (fileB, *), dim*dim, end_time - start_time 

     call random_seed( PUT=seeds )
     call random_number( A )
     call cpu_time( start_time )
     call LU_inplace_forall_2( A )
     call cpu_time( end_time )
     write (fileC, *), dim*dim, end_time - start_time 
     
     deallocate( A )
     dim = dim * 2   
  end do

  close( unit=fileA )
  close( unit=fileB )
  close( unit=fileC )

end program main
