program main
  use LU
  implicit none
  
  integer, parameter         :: dim = 3
  real(kind=rk), allocatable :: A(:,:), Ap(:,:)
  real(kind=rk)              :: L(dim,dim), U(dim,dim)
  integer                    :: i, j

  allocate( A(dim,dim), Ap(dim,dim) )
  A = reshape( (/ 1,2,3, 2,-1,4, 3,1,-1 /), (/ 3, 3 /) )
  call printout( A )
  Ap = A; call LU_inplace_forall(Ap) ! A --> A’
  call printout( Ap )
  L = Lower(Ap)
  call printout( L )
  ! Extract lower triangle of A’
  U = Upper(Ap)
  call printout( U )
  ! Extract upper triangle of A’
  Ap = matmul(L,U)
  ! now Ap gets a new meaning
  call printout( Ap )
  do j = 1, dim; do i = 1, dim
     if (abs(Ap(i,j) - A(i,j)) > 1.0e-07 ) then
        print *, "ERROR", i,j, Ap(i,j), A(i,j)
        stop 1
     endif
  end do; end do

contains
  subroutine printout( m )
    real(kind=rk) :: m(dim,dim)
    integer       :: i
    do i=1, dim
       print *, m(i,:)
    end do
    print *,""
  end subroutine printout
end program main

