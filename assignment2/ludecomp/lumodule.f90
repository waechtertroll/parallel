module LU
  implicit none

  integer, parameter :: rk = selected_real_kind( 12 )

contains

  subroutine LU_inplace( A )
    real(kind=rk)              :: A(:,:)
    integer                    :: i, j, k, n
    n = size( A(1,:) )
    do k = 1, n-1  ! copy-paste from assignment sheet
       do i = k+1, n
          A(i, k) = A(i, k) / A(k, k)
       end do
       do i = k+1, n
          do j = k+1, n
             A(i, j) = A(i, j) - A(i, k)*A(k, j)
          end do
       end do
    end do
  end subroutine LU_inplace

  subroutine LU_inplace_forall( A )
    real(kind=rk)              :: A(:,:)
    integer                    :: i, j, k, n
    n = size( A(1,:) )
    do k = 1, n-1  ! copy-paste from assignment sheet
       forall( i=k+1:n )
          A(i, k) = A(i, k) / A(k, k)
       end forall
!       do i = k+1, n
!          do j = k+1, n
       forall( i=k+1:n, j=k+1:n )
          A(i, j) = A(i, j) - A(i, k)*A(k, j)
       end forall
!          end do
!       end do
    end do
  end subroutine LU_inplace_forall

  subroutine LU_inplace_forall_2( A )
    real(kind=rk)              :: A(:,:)
    integer                    :: i, j, k, n
    n = size( A(1,:) )
    do k = 1, n-1   ! can't be parallelized because k can't be used by another forall
                    ! and because of the sequential access to array elements
       A(k+1:n, k) = A(k+1:n, k) / A(k, k)
       forall (i=k+1:n, j=k+1:n)
          A(i, j) = A(i, j) - A(i, k) * A(k, j)
       end forall
    end do
  end subroutine LU_inplace_forall_2

  function lower( A ) result( L )
    real(kind=rk), intent(in) :: A(:,:)
    real(kind=rk)             :: L( size(A(:,1)), size(A(1,:)) )
    integer                   :: i, j, n
    n = size( A(1,:) )
    L = A
    forall( i=1:n)
      forall( j=i+1:n )
         L(i,j) = 0.0
       end forall
       L(i,i) = 1.0
    end forall
  end function lower

  function upper( A ) result( U )
    real(kind=rk), intent(in) :: A(:,:)
    real(kind=rk)             :: U( size(A(:,1)), size(A(1,:)) )
    integer                   :: i, j, n
    U = A
    n = size( A(1,:) )
    forall( i=1:n )
       forall( j=1: i-1 )
          U(i,j) = 0.0
       end forall
    end forall
  end function upper

end module LU
