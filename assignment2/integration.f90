program IntegrationMain
  use MyIntegration
  implicit none
  
  interface
     real function myfunc( x )
       real, intent(in) :: x
     end function myfunc
  end interface

  real :: a=0, b=10

  print *, "Result: ", integrate( myfunc, a, b, 2000 )

end program IntegrationMain

real function myfunc( x )
  real, intent(in) :: x
  myfunc = x*x
end function myfunc
