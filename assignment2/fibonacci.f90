program fibonacci
  implicit none
  integer           :: n

  print *, "Enter a positive integer"
  read *, n
  if ( n < 0 ) n = -n

  print *, "Fibonacci series:"
  call fib( n )
  print *, "Facultative:"
  print *, fak( n )

  contains
  subroutine fib( max )     ! iterative
    integer              :: n_minus_1, n_minus_2, n, nn
    integer,intent(in)   :: max
    
    n = 0; n_minus_1 = 0; n_minus_2 = 0
  
    do nn=0, max
       if ( nn == 1 ) n = 1
       print *, nn, n
       n_minus_2 = n_minus_1; n_minus_1 = n
       n = n_minus_1 + n_minus_2
    end do
  end subroutine fib

  recursive function fak( n ) result( f )
    integer, intent(in)                 :: n
    integer(kind=selected_int_kind(10)) :: f
    
    if ( n > 1 ) then 
       f = n * fak( n-1 )
    else 
       f = 1
    end if
  end function fak

end program fibonacci

