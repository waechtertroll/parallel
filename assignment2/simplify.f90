program simplify
  implicit none
  integer, parameter                 :: d=4       ! specify dimensions, for testing
  real, dimension( d, d ), target    :: a, b, c
  real, dimension( :,: ),  pointer   :: aa,bb,cc

  integer                            :: i,j,k

  a = reshape( (/ (i, i=1, d*d) /), (/ d, d /) )
  b = a / 10
  c = a * 10

  do i=1, d
     print *, a(i,:)
  end do

  j = 2; k = d-1;        ! point to maximum indexable subarrays of a/b/c

  !  a(j:k+1, j-1:k) = a(j:k+1, j-1:k) + a(j:k+1, j-1:k)  
  !  a(j:k+1, j-1:k) = b(j:k+1, j-1:k) + c(j:k+1, j-1:k) + c(j:k+1, j:k+1)

  aa => a(j:k+1, j-1:k)
  bb => b(j:k+1, j-1:k)
  cc => c(j:k+1, j-1:k)
  
  aa = aa + aa
  print *, "First addition"
  do i=1, d
     print *, a(i,:)
  end do

  aa = bb + cc + c(j:k+1, j:k+1)    ! no subarray pointer specified for last term
  print *, "Second addition\n"
  do i=1, d
     print *, a(i,:)
  end do

end program simplify
