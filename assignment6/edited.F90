Program fix_me

  ! This program contains (at least) three bugs.
  ! Please find, fix and explain them.

  use omp_lib
  implicit none
  
  integer, parameter  :: n = 50, master = 0
  integer             :: i, nthreads, me
  real, allocatable   :: a(:), b(:), c(:)
  real                :: r

  !-- Initialise vectors ---------------------------

  allocate(a(n), b(n), c(n))

  !$omp parallel default(shared) private(i,me,c)
  ! private r => r scalarproduct gives different results (different private rs
  ! look "global" to multiplication called from every thread)
  ! shared c => c(i) is set by last calling method (output dependent)
  !$omp do
  do i = 1, size(a)
     a(i) = i * 1.0
     b(i) = a(i)
  end do
  !$omp end do

  !$omp master
  nthreads = omp_get_num_threads()
  print *, 'Number of threads = ', nthreads
  !$omp end master

  !--Put some barriers to have clean output---------

  me = omp_get_thread_num()
  call scalarprod(r,a,b)

  !$omp master
  print *, 'Scalar product a*b =  ', r
  !$omp end master

  !$omp barrier
  print *, 'Thread ',me,' starting sections...'
  !$omp barrier
  
  !$omp sections
  !-------------------------------------------------
  !$omp section
  do i = 1, n
     c(i) = a(i) * b(i)
  enddo
  call print_c(c, me, 1)

  !-------------------------------------------------
  !$omp section
  do i = 1, n
     c(i) = a(i) + b(i)
  end do
  call print_c(c, me, 2)
  !$omp end sections

  !--Put here another barrier to have clean output---

  !$omp barrier
  print *, 'Thread', me,' all done, exiting...'

  !$omp end parallel

  deallocate(a,b,c)

contains

  subroutine print_C(c, me, sec)

    integer, intent(in)  :: me, sec
    real,    intent(in)  :: c(:)
    integer              :: i

    !---Use critical region for clean output--------
    !$omp critical
    print *, ' '
    print *, 'Thread', me,' did section', sec
    do i=1, size(c)
       print "(e12.6, $)", c(i)
       if ( mod(i,6) == 0) print *, ' '
    end DO
    print *, ' '
    !$omp end critical

    ! barrier 1) not neccessary, end critical does that
    !         2) hangs if threads > 2
    print *,'Thread',me,' done and synchronized'

  end subroutine print_c

  !-------------------------------------------------
  subroutine scalarprod(r,a,b)

    real,    intent(in)  :: a(:), b(:)
    integer              :: i
    real                 :: r

    !$omp do reduction(+:r)
    do i = 1, size(a)
       r = r + a(i) * b(i)
    enddo

  end subroutine scalarprod
  
end Program fix_me
