/*
 * prints energy levels to "data.dat"
 * first param: dt, second param: stop time
 */

#include <stdio.h>
#include <stdlib.h>
#include "constants.h"


void vv_init( double* ppos, const int* n, const double* w, const double* h );
void update( double* ppos, double dt, double *U, double *K );

int main( int argc, char **argv ) {
  double *ppos;
  double goal = 5.0;
  double V, K, t;
  FILE *output = fopen( "data.dat", "w" );
  V = K = t = 0.0;
  if ( argc > 1 ) dt = atof( argv[1] );
  if ( argc > 2 ) goal = atof( argv[2] );
  else goal = dt * 1e3;
  ppos = malloc( dim*n*sizeof( double ) );
  vv_init( ppos, &n, &areaWidth, &areaHeight );
  while( t < goal ) {
    update( ppos, dt, &V, &K );
    //    printf( "%f\t%f\t%f\t%f\n", t, V, K, V+K );
    fprintf( output, "%f\t%f\t%f\t%f\n", t, V, K, V+K );
    t += dt;
  }
  fclose( output );
  return 0;
}
    
