#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

/*
 * Parameters for simulation
 */
#include "constants.h"

/* 
 * Fortran functions,
 */
void vv_init( double* ppos, const int* n, const double* w, const double* h );
void update( double* ppos, double dt, double *U, double *K );
void cleanup();

/*
 * Graphics functions forward declarations
 */
double *ppos;
double U = 0.0, K = 0.0, E = 0.0, scale;
void initGL();                              // sets standard values
void display();                             // updates the particle positions and draws
void idle();                                // calls update and redraw
void reshape( int, int );                   // called on init and theoretical window resize
void keyboard( unsigned char, int, int );   // so ESC can quit
void drawBox();

/*==============================================================================
 *   main
 *==============================================================================
 */
int main( int argc, char **argv ) {
  if( argc > 1 ) { // read delta from first argument
    dt = atof( argv[1] );
  }

  /* initialise graphics and keyboard */
  glutInit( &argc, argv );
  glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA );
  glutInitWindowSize( (int)screenX, (int)screenY );
  glutCreateWindow( "velocity Verlet" );
  glutReshapeFunc( reshape );
  glutDisplayFunc( display );
  glutKeyboardFunc( keyboard );
  glutIdleFunc( idle );
  initGL();

  /* initialise particle data */
  ppos = malloc( dim*n*sizeof( double ) );
  vv_init( ppos, &n, &areaWidth, &areaHeight );

  /* start display and main loop */
  glutMainLoop();
  return 0;
}

/*==============================================================================
 * update & draw
 *==============================================================================
 */

void display() {
  // update particle positions
  update( ppos, dt, &U, &K );

  if ( E == 0.0 ) { // initialise energy to be conserved after first update
    E = U + K;
    scale = areaHeight/E*0.7;
  }

  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); // clear screen
  GLfloat x1 = areaWidth*1.025, x2 = areaWidth*1.075;
  GLfloat y1 = U*scale, y2 = U*scale+K*scale;

  glBegin( GL_QUADS ); // all vertices from now define 4-point polygons
  glColor3f( 0.2, 0.2, 0.2);
  // draw background
    glVertex2f( 0.0, 0.0 );
    glVertex2f( areaWidth, 0.0 );
    glVertex2f( areaWidth, areaHeight );
    glVertex2f( 0.0, areaHeight );
  // draw background of energy meter
  glVertex2f( x1, 0.0 );
  glVertex2f( x2, 0.0 );
  glVertex2f( x2, areaHeight );
  glVertex2f( x1, areaHeight );
  // draw e_pot meter
  glColor3f( 1.0, 0.0, 0.0 );
  glVertex2f( x1, 0.0 );
  glVertex2f( x2, 0.0 );
  glVertex2f( x2, y1 );
  glVertex2f( x1, y1 );
  // draw e_kin meter
  glColor3f( 0.0, 1.0, 0.0 );
  glVertex2f( x1, y1 );
  glVertex2f( x2, y1 );
  glVertex2f( x2, y2 );
  glVertex2f( x1, y2 );
  glEnd(); // done drawing quads
  if( dim == 3 ) { 
    drawBox();
  }
  // draw points
  glColor3f( 1.0, 0.5, 0.5 );
  glEnableClientState( GL_VERTEX_ARRAY );    // copy all positions to graphics 
  glVertexPointer( dim, GL_DOUBLE, 0, ppos ); //   card memory and render them
  glDrawArrays( GL_POINTS, 0, n );           //   in one go
  glDisableClientState( GL_VERTEX_ARRAY );   // (much faster than for-loop)
  glutSwapBuffers();                         // double buffering
}

/*==============================================================================
 * GL and program flow helper functions
 *==============================================================================
 */

// sets up GL states
// note that GL_DEPTH_TEST is not enabled as the scene is too simple
// to waste gpu-/cpu time on intersection tests
void initGL() {
  glClearColor( 0.0, 0.0, 0.0, 0.0 ); // background color
  glPointSize( 5.0 );
  glEnable( GL_POINT_SMOOTH ); // make round points if graphics card can do so
}

void idle() {
  glutPostRedisplay();  // if display() is finished, call it again
}

// corrects perspective when window has been resized
void reshape ( int w, int h ) {
  GLfloat width = ( GLfloat ) w;
  GLfloat height = ( GLfloat ) h;
  glViewport ( 0, 0, ( GLsizei ) width, ( GLsizei ) height );
  glMatrixMode ( GL_PROJECTION );
  glLoadIdentity();
  gluPerspective(45.0, (GLfloat) width/(GLfloat) height, 0.0, 500.0);
  gluLookAt( areaWidth * 0.535, areaHeight * 0.51, 2,  // "eye" position
	     areaWidth * 0.535, areaHeight * 0.51, 0.0, // center of view
	     0.0, 1.0, 0.0 );                           // direction of "up"
  glMatrixMode( GL_MODELVIEW );
  glScalef( 1.0, 1.0, -1.0 );   // invert z-axis
}

// called if any key is hit, x and y are window positions of the mouse pointer
void keyboard( unsigned char key, int x, int y ) {
  if( key == 27 ) { // ESC quits
    free( ppos );
    cleanup();
    exit( 0 );
  }
}

inline void drawBox() {
  // save view matrix
  glPushMatrix();
  glColor3f( 0.8f, 0.8f, 0.8f );
  // move view matrix as the cube is drawn from the center
  glTranslatef( areaWidth*0.5, areaHeight*0.5, 0.5 );
  // distort view matrix, so cube appears as a cuboid
  glScalef( areaWidth, areaHeight, 1.0 );
  glutWireCube( 1.0 );
  // restore saved matrix without translation and distortion
  glPopMatrix();
}
  
