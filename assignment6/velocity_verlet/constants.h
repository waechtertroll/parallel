#ifndef CONSTANTS_H
#define CONSTANTS_H

const double areaWidth = 2.0, areaHeight = 1.5;  // size of field
double dt = 0.001;                               // time step (s)
const double screenX = 800.0, screenY = 600.0;   // window size 
const int n = 250;                               // number of particles

#ifdef DIM2
const int dim = 2;
#else
const int dim = 3;
#endif

#endif
