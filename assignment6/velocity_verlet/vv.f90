module VelocityVerlet
  use iso_c_binding
  use omp_lib
  implicit none

  real(c_double)             :: boxsize(3)   ! dimensions of simulation volume
  integer(c_int)             :: n            ! number of particles
  real(c_double),allocatable :: v(:,:)       ! particle velocities at t
  real(c_double),allocatable :: a(:,:)       ! particle positions at t
  real,parameter             :: pihalf = 3.14159265358979323846/2.0
#ifdef DIM2
  integer,parameter          :: dim = 2
#else
  integer,parameter          :: dim = 3
#endif

contains

!===============================================================================
! initialisation routine
!===============================================================================

  ! initialises C array with random start positions
  ! x        array to randomize, dim*n values
  ! n        number of particles
  ! width    width of box
  ! height   height of box
  subroutine vv_init( x, num, width, height ) bind( c )
    integer(c_int),intent(in)          :: num
    real(c_double),intent(inout)       :: x(dim,num)
    real(c_double),intent(in)          :: width, height
    integer                            :: i

    ! setup parameters and arrays
    n = num
    boxsize = (/ width, height, 1.0_c_double /)
    allocate( v(dim,num), a(dim,num) )
    ! set start values
    v = 0.0; a = 0.0
    call random_number( x )
    ! fit random start positions inside whole box
    do i=1, dim
       x(i,:) = x(i,:) * boxsize(i)
    end do
  end subroutine vv_init


!===============================================================================
! update routine
!===============================================================================

  ! updated positions of all particles with velocity Verlet algorithm
  ! x        array with particle positions
  ! dt       delta time
  ! U, K     energies to be returned
  subroutine update( x, dt, U, K ) bind(c)
    real(c_double),intent(inout)    :: x(dim,n)
    real(c_double),intent(in),value :: dt
    real(c_double)                  :: ai(dim)     ! acceleration of i from j
    real(c_double)                  :: rij         ! eucl. distance i <-> j
    real(c_double)                  :: uij(dim)    ! unity vector j->i
    integer                         :: i,j
    real(c_double)                  :: U, K

    U = 0.0; K = 0.0

    ! calculate positions, velocities and accelerations using VV
    ! assuming mass = 1.0 so F === a
    !$omp parallel do private(rij,ai,uij) reduction(+:K) reduction(+:U)
    do i=1, n
       ! update position
       x(:,i) = x(:,i) + v(:,i)*dt + 0.5*a(:,i)*dt*dt
       ! do reflection
       do j=1, dim
          if ( x(j,i) < 0.0 ) then
             x(j,i) = -x(j,i)
             v(j,i) = -v(j,i)
          elseif ( x(j,i) > boxsize(j) ) then
             x(j,i) = 2*boxsize(j) - x(j,i)
             v(j,i) = -v(j,i)
          end if
       end do
       
       ! calculate a at time t + dt
       ai = 0
       do j=1, n
          if( i /= j) then
             rij = min( sqrt( sum( (x(:,j)-x(:,i))**2 ) ), pihalf )
             uij = (x(:,j)-x(:,i)) / rij
             ai = ai - uij * sin( rij ) * cos( rij ) * 2.0
             U = U + sin( rij ) ** 2 ! U at time t
          end if
       end do
       K = K + dot_product( v(:,i), v(:,i) ) ! K at time t
       v(:,i) = v(:,i) + 0.5 * (ai + a(:,i)) * dt
       a(:,i) = ai
    end do

    K = 0.5 * K
    U = 0.5 * U
  end subroutine update


!===============================================================================
! free memory on exit
!===============================================================================

  subroutine cleanup() bind(c)
    deallocate( v )
    deallocate( a )
  end subroutine cleanup

end module VelocityVerlet
