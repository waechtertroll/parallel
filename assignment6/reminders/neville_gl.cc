#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstdio>
#include "polyNeville.h"

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//#include "uebung_3.2_set1.h"
#include "uebung_3.2_set2.h"

const int steps ( 100 );
const int order ( 2 );

double arrMax ( const double* a, const int n );
double arrMin ( const double* a, const int n );

GLdouble width, height;
int wd;

double xPoints[steps];
double yPoints[steps];

/** Interpoliert die Eingangsdaten mit dem Neville-Algorithmus aus und speichert die x- und y-Werte in xPoints und yPoints */
void init() {
    width  = 800.0;
    height = 600.0;

    /** Aus polyNeville_start.cc **/

    // calculate range, stepsize
    const double xrange = fabs ( xdata[0] - xdata[dsize-1] );
    const double xlow = MIN ( xdata[0], xdata[dsize-1] ) - .1 * xrange;
//    const double xhigh = MAX ( xdata[0], xdata[dsize-1] ) + .1 * xrange;
    const double stepSize ( 1.2*xrange / steps );

    printf ( "#datasize: %i\n", dsize );
    for ( int i ( 0 ); i < dsize; ++i )
        printf ( "#data: %e %e\n", xdata[i], ydata[i] );
    printf ( "\n#approximation order: %i\n", order );

    // do interpolation
    double x = xlow;

    for ( int i ( 0 ); i <= steps; i++ ) {
        double dy;
        double y = polyinterp ( dy, x, order, xdata, ydata, dsize );
        // Speichern der Werte für Bildschirmausgabe
        xPoints[i] = x;
        yPoints[i] = y;

        printf ( "%e\t%e\t%e\n", x, y, dy );

        x += stepSize;
    }

    /** ENDE polyNeville_start.cc **/
}

/** Bildschirmausgabe */
void display ( void ) {
    glClearColor ( 1.0, 1.0, 1.0, 0.0 );
    glClear ( GL_COLOR_BUFFER_BIT );
    GLdouble minX = ( GLdouble ) ( xPoints[0] - 1.0 ), maxX = ( GLdouble ) ( xPoints[steps-1] + 1.0 );
    GLdouble minY = ( GLdouble ) ( arrMin ( yPoints, steps ) - 1.0 ), maxY = ( GLdouble ) ( arrMax ( yPoints, steps ) + 1.0 );
    GLdouble xFac = width / ( maxX - minX ), yFac = height / ( maxY - minY );
    glTranslated ( -minX*xFac, -minY*yFac, 0 );

    // Zeige Stützpunkte
    glColor3f ( 1.0f, 0.5f, 0.5f );
    glBegin ( GL_TRIANGLES );
    for ( int i = 0; i < dsize; i++ ) {
        glVertex2d ( xdata[i]*xFac, ydata[i]*yFac );
        glVertex2d ( xdata[i]*xFac + 20, 0 );
        glVertex2d ( xdata[i]*xFac - 20, 0 );
    }
    glEnd();

    // Zeichne interpolierte Kurve
    glColor3f ( 0.0, 0.0, 0.0 );
    glBegin ( GL_LINES );
    for ( int i = 2; i < steps; ++i ) {
        glVertex2d ( xPoints[i-1]*xFac, yPoints[i-1]*yFac );
        glVertex2d ( xPoints[i]*xFac, yPoints[i]*yFac );
    }
    glEnd();

    glFlush();
}

/** Wird aufgerufen, wenn die Bildschirmgröße geändert wird */
void reshape ( int w, int h ) {
    width = ( GLdouble ) w;
    height = ( GLdouble ) h;
    glViewport ( 0, 0, ( GLsizei ) width, ( GLsizei ) height );
    glMatrixMode ( GL_PROJECTION );
    glLoadIdentity();
    gluOrtho2D ( 0.0, width, 0.0, height );
}

int main ( int argc, char *argv[] ) {
    init();
    glutInit ( &argc, argv );
    glutInitDisplayMode ( GLUT_SINGLE | GLUT_RGBA );
    glutInitWindowSize ( ( int ) width, ( int ) height );
    wd = glutCreateWindow ( "Polynominterpolation mit dem Neville-Algorithmus" );
    glutReshapeFunc ( reshape );
    glutDisplayFunc ( display );
    glutMainLoop();
    return 0;
}

/** Findet größte double in einem Array */
double arrMax ( const double* a, const int n ) {
    double max = a[0];
    for ( int i = 0; i < n; i++ )
        if ( a[i] > max ) max = a[i];
    return max;
}
/** Findet kleinste double in einem Array */
double arrMin ( const double* a, const int n ) {
    double min = a[0];
    for ( int i = 0; i < n; i++ )
        if ( a[i] < min ) min = a[i];
    return min;
}
