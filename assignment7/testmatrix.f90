module matrixTest
  use iso_c_binding
  implicit none

contains

  ! returns 1 if matrix OK, else 0
  subroutine check_mul( dim, A, B, C1, res ) bind( c )
    integer(c_int),intent(in),value    :: dim
    integer(c_int),dimension(dim,dim)  :: A, B, C1, C2
    integer(c_int)                     :: res

    C2 = matmul( transpose(A), transpose(B) )
    res = 1
    if( any(transpose(C1) /= C2) ) then
       res = 0
    endif
    
  end subroutine check_mul

end module matrixTest
