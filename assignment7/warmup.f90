program WarmUpMPI
  use mpi
  implicit none

  integer                               :: me, npe
  integer                               :: ierror
  character(len=MPI_MAX_PROCESSOR_NAME) :: pname

  call MPI_Init( ierror )

  call MPI_Comm_size( MPI_COMM_WORLD, npe, ierror )
  call MPI_Comm_rank( MPI_COMM_WORLD, me, ierror )
!  call MPI_Get_processor_name( pname, MPI_MAX_PROCESSOR_NAME, ierror )

  if ( me == 0 ) then
     print *, "Machines used:", npe
  end if

  print *, "Hello World from node", me, "My name is ", pname

  call MPI_Finalize( ierror )
end program WarmUpMPI
