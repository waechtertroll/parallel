#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>

int main() {
  int me, npe, prev, next, tag=8;
  MPI_Status status;
  const int l = 13;
  char msg[14] = "MPI ist toll!\0";

  MPI_Init( 0, NULL );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );
  prev = (me > 0) ? me-1 : npe-1;
  next = (me < npe-1) ? me+1 : 0;
  srand( me );
  
  if (me == 0) {
    MPI_Ssend( msg, l, MPI_CHAR, next, tag, MPI_COMM_WORLD );
    MPI_Recv( msg, l, MPI_CHAR, prev, tag, MPI_COMM_WORLD, &status );
    printf( "I heard %s from %d\n", msg, prev );
  }
  else {
    MPI_Recv( msg, l, MPI_CHAR, prev, tag, MPI_COMM_WORLD, &status );
    msg[(int)random()%l] = '?';
    MPI_Ssend( msg, l, MPI_CHAR, next, tag, MPI_COMM_WORLD );
  }
  MPI_Finalize();
  return 0;
}
