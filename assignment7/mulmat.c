#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <time.h>

const int dim=9;

int *A, *B, *C;
int *bFrag, *cFrag;
int me, npe, tag=8, resOk;
int fsize;     // size of fragment per PE
MPI_Status status;

void printMatrix( int*, int rows, int cols );
int mij( int i, int j );
void check_mul( const int, int*, int*, int*, int* );

int main() {
  MPI_Init( 0, NULL );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );

  if (dim % npe != 0) { // simplify distribution
    printf( "Error: dim must be multiple of PE count!\n" );
    printf( "--> dim=%d, nPE=%d\n", dim, npe );
    MPI_Abort( MPI_COMM_WORLD, 0 );
  }

  // allocate memory on all nodes
  A = malloc( dim*dim * sizeof( int ) );
  fsize = dim/npe;
  bFrag = malloc( dim*fsize * sizeof( int ) );
  cFrag = malloc( dim*fsize * sizeof( int ) );
  for( int i=0; i < dim*fsize; i++ )
    cFrag[i] = 0;

  /*===========================================================================
   * Master
   *===========================================================================*/
  if (me == 0) {
    printf( "Multiplying matrices %dx%d...\n\n", dim, dim );
    B = malloc( dim*dim * sizeof( int ) );
    C = malloc( dim*dim * sizeof( int ) );
    for( int i=0; i < dim*dim; i++ )
      C[i] = 0;
    srand( time( NULL ) );
    for( int i = 0; i < dim*dim; i++ ) {
      A[i] = random() % 100;
      B[i] = random() % 100;
    }

    printMatrix( A, dim, dim );
    printMatrix( B, dim, dim );

    // distribute data
    int *buf = malloc( dim*fsize * sizeof( int ) );
    for( int pe=1; pe < npe; pe++ ) {
      printf( "master: A => %d\n", pe );
      MPI_Ssend( A, dim*dim, MPI_INTEGER, pe, tag, MPI_COMM_WORLD );
      for( int i=0; i < fsize; i++ )
	for( int j=0; j < dim; j++ )
	  buf[mij(i,j)] = B[mij(j,i+pe*fsize)];
      printf( "master: B[%d-%d] => %d\n\n", pe*fsize, (pe+1)*fsize-1, pe );
      MPI_Ssend( buf, dim*fsize, MPI_INTEGER, pe, tag, MPI_COMM_WORLD );
    }

    // multiplicate row-wise
    for( int k=0; k<fsize; k++ ) // calculate k-th row
      for( int i=0; i < dim; i++ )
	for ( int j=0; j < dim; j++ )
	  C[mij(i,k)] += A[mij(i,j)] * B[mij(j,k)];

    // collect results
    for( int pe=1; pe < npe; pe++ ) {
      printf( "master: C[%d-%d] <= %d\n", pe*fsize, (pe+1)*fsize - 1, pe );
      MPI_Recv( buf, dim*fsize, MPI_INTEGER, pe, tag, MPI_COMM_WORLD, &status );
      for( int i=0; i < dim; i++ )
      	for( int j=0; j < fsize; j++ )
	  C[mij(i, pe*fsize + j)] = buf[mij(j,i)];
    }
    free( buf );

    printMatrix( C, dim, dim );

    // check multiplication
    printf( "\nmaster: Testing result using Fortran matmul()...\n" );
    check_mul( dim, A, B, C, &resOk );
    if( resOk )
      printf( "        --> Multiplication successfull!\n" );
    else
      printf( "        --> Error in multiplication\n" );

    free( B );
    free( C );
  }
  /*===========================================================================
   * others
   *===========================================================================*/
  else {
    // receive data
    MPI_Recv( A, dim*dim, MPI_INTEGER, 0, tag, MPI_COMM_WORLD, &status );
    MPI_Recv( bFrag, dim*fsize, MPI_INTEGER, 0, tag, MPI_COMM_WORLD, &status );
    printMatrix( A, dim, dim );

    // multiplicate; note columns are stored as rows in xFrag!
    for( int k=0; k < fsize; k++ ) // calculate k-th row
      for( int i=0; i < dim;i++ )
	for( int j=0; j < dim; j++ )
	  cFrag[k*dim+i] += A[mij(i,j)] * bFrag[k*dim+j];

    // send back results
    MPI_Ssend( cFrag, dim*fsize, MPI_INTEGER, 0, tag, MPI_COMM_WORLD );
  }

  free( A );
  free( bFrag );
  free( cFrag );

  MPI_Finalize();
  return 0;
}

void printMatrix( int *m, int rows, int cols )
{
  if (dim >= 10) return;
  for( int r=0; r < rows; r++ ) {
    for( int c=0; c < cols; c++ ) 
      printf( "%8ld", m[mij(r,c)] );
    printf( "\n" );
  }
  printf( "\n" );
}

inline int mij( int i, int j ) {
  return dim * i + j;
}
