#include <stdio.h>
#include <mpi.h>

int main() {
  const int BUF_LEN = 6;
  char input = 'y';
  int count = 1;
  int me, other, sender=0, tag = 8;
  MPI_Status status;
  char buf[BUF_LEN];
  char msg[2][6] = { "Ping!\0", "Pong!\0" };

  MPI_Init( 0, NULL );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  other = (me+1) % 2;

  printf( "\nHello, my ID is %d and I must send %s to %d\n", me, msg[me], other );
  while ( count < 10 ) {
    printf( "cycle %d\n", count++ );
    if (me == sender) {
      printf( "\t%d sending %s to %d\n", me, msg[me], other );
      MPI_Ssend( msg[me], BUF_LEN, MPI_CHAR, other, tag, MPI_COMM_WORLD );
    }
    else {
      MPI_Recv( buf, BUF_LEN, MPI_CHAR, other, tag, MPI_COMM_WORLD, &status );
      printf( "\t%d received %s from %d\n", me, buf, sender );
    }
    sender = (sender+1) % 2;
    if ( sender == 0 && me == sender ) {
      printf( "Once more? " );
      //      scanf( "%c", &input );
    }
     printf( "\tswitching comm to %d -> %d\n", sender, (sender+1)%2 );
     MPI_Barrier( MPI_COMM_WORLD );
  }
  //  MPI_Abort( MPI_COMM_WORLD, 0 );
  MPI_Finalize();
  return 0;
}
