#include <mpi.h>
#include <stdio.h>

int main() {
  int me, npe;
  int mpn = MPI_MAX_PROCESSOR_NAME;
  char pname[mpn];

  MPI_Init( 0, NULL );
  MPI_Get_processor_name( pname, &mpn );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );

  printf( "Hello World from PE #%d, \"%s\"\n", me, pname );

  MPI_Barrier( MPI_COMM_WORLD );
  MPI_Finalize();
  if ( me == 0 ) printf( "\nAll %d PEs greeted politely. Exiting.\n", npe );
  return 0;
}
