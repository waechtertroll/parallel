program MCIntegration
  use MonteCarlo
  implicit none

  real             :: cborders(2,2), sborders(2,3)
  real, parameter  :: r=1.0
  real             :: crcl, sphr
  integer          :: i
  integer(kind=ik) :: N
  real, parameter  :: pi = 3.1415926535, s=4.0/3.0*pi*(r**3), c=pi*(r**2)

  ! set up bounding shapes
  cborders = reshape( (/ -r,r, -r,r /), (/ 2, 2 /) )
  sborders = reshape( (/ -r,r, -r,r, -r,r /), (/ 2, 3 /) )
  N = 10

  do i=1, 8 ! powers of five for stepnum
     crcl = integrate( inCircle, cborders, N )
     sphr = integrate( inSphere, sborders, N )
     print *, N, crcl, abs(c-crcl), sphr, abs(sphr-s)
     N = N*10
  end do

contains
  ! bounding functions for shapes
  
  logical function inCircle( x )
    real, intent(in) :: x(:)
    inCircle = x(1)**2 + x(2)**2 <= r**2
  end function inCircle

  logical function inSphere( x )
    real, intent(in) :: x(:)
    inSphere = x(1)**2 + x(2)**2 + x(3)**2 <= r**2
  end function inSphere

  logical function inCube( x )
    real, intent(in) :: x(:)
    inCube = abs(x(1))<=r .and. abs(x(2))<=r .and. abs(x(3))<=r
  end function inCube

end program MCIntegration
