! calculates integrals for bodys of arbitrary dimensions,
! using "Monte Carlo" random sample method
!
! bounding function of the integrand must be passed!

module MonteCarlo
  use omp_lib
  implicit none

  ! large iteration size leads to big numbers
  integer, parameter :: ik = selected_int_kind(30)
  integer, parameter :: rk = selected_real_kind(8,30)

contains

  ! monte carlo integration
  ! parameters:
  !   inShape    logical function to determine if a vector
  !              is inside the body
  !   borders    real vector of borders constraining the
  !              test volume
  !              each col must be filled with upper,lower ffor
  !              the respective dimension
  !   N          integer number of random samples to use
  real function integrate( inShape, borders, N )
    integer, parameter           :: maxBlock = 10000000
    integer(kind=ik), intent(in) :: N
    real, target, intent(in)     :: borders(:,:)
    real, allocatable, target    :: rnd(:,:)
    integer                      :: dim, i, j, left, m
    real                         :: V
    real, pointer                :: pX(:), pB(:)
    real(kind=rk)                :: INT
    
    interface
       logical function inShape( x )
         real, intent(in) :: x(:)
       end function inShape
    end interface

    dim = size( borders(1,:) ); left = N; V = 1.0

    ! calculate bounding volume (usually not too many
    ! dimensions, so serial instead of parallel overhead)
    do i=1, dim
       pB => borders(:,i)
       V = V * (pB(2) - pB(1))
    end do

    do while (left > 0)
       m = min( left, maxBlock )       ! adapt random block
       allocate( rnd(dim,min(left,m)) )
       left = left - m
       call random_number( rnd )
       
       nullify( pX )
       nullify( pB )
       
       ! adapt sampled points to fit into bounding volume
       !$omp parallel do default(none) private(pX) shared(rnd)
       do i=1, m                 ! every coordinate vector
          pX => rnd(:,i)
          do j=1, dim            ! every element
             pB => borders(:,j)  ! to it's dimensional bounds
             pX(j) = pX(j) * (pB(2)-pB(1)) + pB(1)
          end do
       end do
       !$omp end parallel do
       nullify( pX )
       nullify( pB )
       
       
       ! calculate the integration by checking the sampled points
       !$omp parallel do default(none) shared(rnd) reduction(+:INT)
       do i=1, m
          if (inShape( rnd(:,i) )) INT = INT + 1.0
       end do
       !$omp end parallel do
       deallocate( rnd )
    end do
    INT = INT * V / N
    integrate = INT
  end function integrate
     
end module MonteCarlo
