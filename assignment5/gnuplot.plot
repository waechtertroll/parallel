set autoscale
unset log
unset label
set xtic auto
set ytic auto
set xlabel "n"

set terminal pdf
set output "results.pdf"

plot "results.csv" using 1:2 title "circle area", \
"results.csv" using 1:3 title "circle error" w l, \
"results.csv" using 1:4 title "sphere volume", \
"results.csv" using 1:5 title "circle error" w l