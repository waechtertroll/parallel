program vererbung
  implicit none
  type Motor_T
     integer          :: leistung, hubraum, zylinder
     character(len=6) :: treibstoff
  end type Motor_T

  type Reifen_T
     integer           :: anzahl
     character(len=20) :: fabrikat
     real              :: luftdruck, durchmesser
  end type Reifen_T

  type Fahrzeug_T
     character(len=20) :: fabrikat, farbe, kennzeichen
     integer           :: sitzplaetze
     real              :: gewicht
     type(Reifen_T)    :: reifen
     type(Motor_T)     :: motor
  end type Fahrzeug_T

  type, extends( Fahrzeug_T ) :: Auto_T
     real  :: kofferraum
  end type Auto_T

  type, extends( Fahrzeug_T ) :: Bus_T
     integer :: stehplaetze
  end type Bus_T

  type, extends( Fahrzeug_T ) :: Lkw_T
     real :: ladung
  end type Lkw_T

  type( Auto_T ) :: auto
  type( Bus_T ) :: bus

  auto%reifen%fabrikat = "Flitzi"
  auto%kofferraum = 804.2
  
  bus%stehplaetze = 34
  bus%motor%leistung = 194

  print *,"Auto:", auto%reifen%fabrikat, auto%kofferraum
  print *,"Bus:", bus%stehplaetze, bus%motor%leistung

end program vererbung
     
