program helloworld
  implicit none
  
  integer :: i
  
  do i = 1,30
     print *,"Hello world!"
     if ( modulo( i, 2 ) == 0  ) then
        print *,"------------"
     end if
  end do
end program helloworld
