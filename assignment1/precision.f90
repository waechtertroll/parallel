program precision
  implicit none

  ! a)
  integer( kind = selected_int_kind(2) )      :: i
  integer( kind = selected_int_kind(20) )     :: j
  real( kind = selected_real_kind(5) )        :: r
  real( kind = selected_real_kind(14) )    :: s
  real( kind = selected_real_kind(18, 2) )  :: t

  real( kind = kind(r) ) :: one5
  real( kind = kind(s) ) :: one14
  real( kind = kind(t) ) :: one18

  ! b)
  !i = 10000 -> "Fehler: Arithmetischer Überlauf" -> -fno-range-check :D

  ! c)
  r = 3.14159265358979323846
  s = 3.14159265358979323846
  t = 3.14159265358979323846

  print *,"Kindlevel"
  print *,"i:",kind( i )
  print *,"j:", kind( j )
  print *,"r:", kind( r ), r
  print *,"s:", kind( s ), s
  print *,"t:", kind( t ), t

  ! d)
  one5 = 1.0; one14 = 1.0; one18 = 1.0
  print *,"Kopierte Kindlevel"
  print *,"one5:", 4.*atan(one5), "r:", 4.*atan(r)
  print *,"one14:", 4.*atan(one14), "s:", 4.*atan(s)
  print *,"one18:", 4.*atan(one18), "t:", 4.*atan(t)

  ! e)
  print *,"Epsilons"
  print *,r, (r + 3 * epsilon( r ) ), (r + 0.9 * epsilon( r ) )
  print *,s, (s + 3 * epsilon( s ) ), (s + 0.9 * epsilon( s ) )
  print *,t, (t + 3 * epsilon( t ) ), (t + 0.9 * epsilon( t ) )

end program precision
