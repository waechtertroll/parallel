/*
 * Sheet 8, exercise 2.
 * Reads a field of 1000 doubles from datafile.dat, distributes it to 4 PEs,
 * calculates the squareroot of each double, collects results and writes them
 * to output.dat
 * If datafile.dat doesn't exist it is created and filled with random numbers.
 * Files are accessed in binary mode.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <time.h>

int me, npe, tag=42;
MPI_Status status;
const int M=1000;

/* compares two numbers for deviation of less than or exactly epsilon */
int equalsepsilon( double a, double b, double epsilon ) { return abs( a-b ) <= epsilon; }


int main( int argc, char **argv ) {
  FILE *file;
  double nums[M], roots[M];
  double buf[M/4];
  MPI_Init( 0, NULL );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );

  if (npe != 4 ) {
    printf( "ERROR: node count must be set to exactly 4, is %d\n", npe );
    MPI_Abort( MPI_COMM_WORLD, 1 );
  }

  if (me == 0 ) {

    // Fill array
    file = fopen( "datafile.dat", "r" );
    if (file == NULL )  { // if no file found, write new one
      printf( "Creating data file..." );
      srand( time( NULL ) );
      file = fopen( "datafile.dat", "w+" );
      for( int i=0; i < M; i++ ) nums[i] = random() % 10000;
      fwrite( nums, sizeof( double ), M, file );
      fclose( file );
      printf( "done. Exiting.\n" );
      file = fopen( "datafile.dat", "r" );
    }
    if ( file == NULL ) { // file doesn't exist after creating
      printf( "ERROR: Could not create and reopen file\n!" );
      MPI_Abort( MPI_COMM_WORLD, 1 );
    }
    printf( "datafile.dat exists, reading..." );
    fread( nums, sizeof( double ), M, file );
    fclose( file );
    printf( "done.\n" );
  }

  // distribute data
  MPI_Scatter( nums, M/4, MPI_DOUBLE,
	       buf, M/4, MPI_DOUBLE,
	       0, MPI_COMM_WORLD );
  // calculate roots
  for( int i=0; i < M/4; i++ )
    buf[i] = sqrt( buf[i] );

  // collect results
  MPI_Gather( buf, M/4, MPI_DOUBLE,
	      roots, M/4, MPI_DOUBLE,
	      0, MPI_COMM_WORLD );

  // check results for errors
  if (me == 0) {
    int errors = 0;
    printf( "Checking results...\n" );
    for( int i=0; i < M; i++ ) {
      if ( !equalsepsilon( nums[i], roots[i]*roots[i], 1e-5 ) ) {
	printf( "ERROR in squareroot!\n" );
	errors++;
      }
    }
    printf( "All done. Found %d errors\n", errors );
    // if calculation was correct, write output file
    if (errors == 0) {
      printf( "Writing roots to file.\n" );
      file = fopen( "roots.dat", "w" );
      fwrite( roots, sizeof( double ), M, file );
      fclose( file );
    }
    else printf( "Exiting without saving due to math errors." );
  }

  MPI_Finalize();

  return 0;
}
