/*
 * Sheet 8, ex3.
 * Distribute a NxN matrix of random integers to N PEs and transpose it using
 * MPI_Alltoall.
 * Transposition is to be checked visually.
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <string.h>

int me, npe;
const int dim = 3;

void printMatrix( int* );
void transposeMPI( int* );

int main() {
  MPI_Init( 0, NULL );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );
  if (npe != dim) {
    printf( "ERROR: number of nodes expexted: %d, given: %d\n, exiting.", dim, npe );
    MPI_Abort( MPI_COMM_WORLD, 1 );
  }

  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  
  int m[dim*dim];

  if (me == 0)  {
    srand( time( NULL ) );
    for( int i=0; i < dim*dim; i++ )
        m[i] = random() % 100;
    printMatrix( m );
  }
  transposeMPI( m );
  if (me == 0) printMatrix( m );
  MPI_Finalize();
  return 0;
}

void transposeMPI( int* m ) {
  int buf[dim];
  MPI_Scatter( m, dim, MPI_INTEGER,
	       buf, dim, MPI_INTEGER,
	       0, MPI_COMM_WORLD );
  printf( "%3d: has row(", me );
  for( int i=0; i < dim; i++ )
    printf( "%4d", buf[i] );
  printf( ")\n" );
  MPI_Alltoall( buf, 1, MPI_INTEGER,
		m, 1, MPI_INTEGER,
		MPI_COMM_WORLD );
  memcpy( buf, m, dim * sizeof( int ) );
  printf( "%3d: alltoall()\n", me );
  printf( "%3d: has row(", me );
  for( int i=0; i < dim; i++ )
    printf( "%4d", buf[i] );
  printf( ")\n" );
  MPI_Gather( buf, dim, MPI_INTEGER,
	      m, dim, MPI_INTEGER,
	      0, MPI_COMM_WORLD );
}

void printMatrix( int* m ) {
  printf( "%d has matrix\n", me );
  for( int i=0; i < dim; i++ ) {
    for( int j=0; j < dim; j++ ) {
      printf( "%5d", m[i*dim+j] );
    }
    printf( "\n" );
  }
}
