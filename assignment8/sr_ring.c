/*
 * Sheet 8, ex4.
 * Create fields to store data and shadow copies of neighbour's edge rows/cols
 * and provide a method to exchange shadow copies.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>
#include <string.h>
#include <math.h>

int Lx, Ly, dim=5, bufSize, rowlen;

int me, npe, left, right, up, down, tag=42;
MPI_Status status;
MPI_Datatype MPI_COLUMN, MPI_ROW;

int *phi, *buf;

void printField( int* );
int min( int a, int b ) { return (a<b) ? a : b; }
int myrow( int x ) { return x / (int)sqrt( npe ); }
int mycol( int x ) { return x % (int)sqrt( npe ); }
int rowstart( int x ) { return myrow(x) * (int)sqrt(npe); }
int rowend( int x ) { return min( (myrow(x)+1) * (int)sqrt(npe) - 1, npe-1); }

void exchangeBorders();
void setDtypes();
void calcNeighbours();
void setupFields();
int checkExchange();

int main() {
  MPI_Init( 0, NULL );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );

  calcNeighbours();

// #ifdef DEBUG
//   // print neighbours for debug purposes
//   printf( "%4d: grid position = (%d/%d)\n", me, myrow( me ), mycol( me ) );
//   printf( "   %3d\n%3d%3d%3d\n   %3d\n\n", up, left, me, right, down );
// #endif

  setupFields();
  setDtypes();
  
  exchangeBorders();
#ifdef DEBUG
  printField( phi );
#endif

  // check for success: checkExchange() returns 1 for every successfull PE
  int success=0, mysuccess=checkExchange();
  // sum up number of successfull PEs
  MPI_Reduce( &mysuccess, &success, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD );
  if (me == 0) {
    if (success < npe)
      printf( "ERROR: %d PEs received wrong borders\n", npe-success );
    else
      printf( "All shadow copies created successfully\n" );
  }
  free( phi );
  free( buf );

  MPI_Finalize();

  return npe-success;
}

/*
 * calculate the PEs which are neighbours in all 4 directions
 * results in periodically ordered field of arbitrary number of PEs
 */
void calcNeighbours() {
  rowlen = (int)sqrt( npe );
  left = (me == rowstart( me )) ? rowend( me ) : me-1;
  right = (me == rowend( me )) ? rowstart( me ) : me+1;
  up = (me > rowend( 0 )) ? me-rowlen : rowend( npe-1 )-rowlen+me+1;
  if (up >= npe) up -= rowlen; // correct non-square rings
  down = (me < npe-rowlen ) ? me+rowlen : me%rowlen;
}

/*
 * allocate memory and creates values for this PE's local field
 * also sets up byte buffer size for memory operations
 */
void setupFields() {
  // complete data field contains dim x dim for each processor
  Lx = rowlen*dim;
  Ly = npe/rowlen*dim;

  bufSize = (dim+2) * (dim+2) * sizeof( int );
  phi = malloc( bufSize );
  buf = malloc( bufSize );

  for( int i=0; i < (dim+2) * (dim+2); i++ )
    phi[i] = me;
  memcpy( buf, phi, bufSize );

}

/*
 * create and commit MPI datatypes for rim vectors
 */
void setDtypes() {
  // RandUD is simply dim successive integers
  MPI_Type_contiguous( dim, MPI_INTEGER, &MPI_ROW );
  MPI_Type_commit( &MPI_ROW );
  // RandLR is a column of single integers which are stored dim+2 positions apart
  MPI_Type_vector( dim, 1, dim+2, MPI_INTEGER, &MPI_COLUMN );
  MPI_Type_commit( &MPI_COLUMN );
}

/*
 * exchange shadow copies of all PEs' rim elements by copying row/col vectors
 * from and to specific start positions inside the lexically addressed array
 */
void exchangeBorders() {
  memcpy( buf, phi, bufSize ); // write central elements of phi to buffer
  // send first row from second index of phi to last row, second index of recvbuf
  MPI_Sendrecv( phi+1, 1, MPI_ROW, up, tag,
		buf+(dim+1)*(dim+2)+1, 1, MPI_ROW, down, tag,
		MPI_COMM_WORLD, &status );
  // send last row from second index of phi to first row, second index of recvbuf
  MPI_Sendrecv( phi+(dim+1)*(dim+2)+1, 1, MPI_ROW, down, tag,
		buf+1, 1, MPI_ROW, up, tag,
		MPI_COMM_WORLD, &status );
  // send second col from second row of phi to second-last col, second row of recv
  MPI_Sendrecv( phi+(dim+2)+1, 1, MPI_COLUMN, left, tag,
		buf+(dim+2)+dim+1, 1, MPI_COLUMN, right, tag,
		MPI_COMM_WORLD, &status );
  // send second-to-last col from row2 of phi to second col, row2 of recvbuf
  MPI_Sendrecv( phi+(dim+2)+dim+1, 1, MPI_COLUMN, right, tag,
		buf+(dim+2), 1, MPI_COLUMN, left, tag,
		MPI_COMM_WORLD, &status );
  memcpy( phi, buf, bufSize ); // write buffer with changed shadow borders back
}

/*
 * write field with shadow elements to stdout
 */
void printField( int *f ) {
  printf( "%d: has data\n", me );
  for( int i=0; i < dim+2; i++ ) {
    for( int j=0; j < dim+2; j++ )
      printf( "%3d", f[i * (dim+2) + j] );
    printf( "\n" );
  }
  printf( "\n" );
}

/*
 * test if exchange was correct
 */
int checkExchange() {
  for( int i=1; i <= dim; i++ ) { // note that i starts from 1, not zero
    if ( phi[i] != up ||
	 phi[(dim+1) * (dim+2) + i] != down ||
	 phi[i * (dim+2)] != left ||
	 phi[i * (dim+2) + dim + 1] !=right )
      return 0;
  }
  return 1;
}
