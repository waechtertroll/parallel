/*
 * Sheet 8, ex 1.
 * Calculate pi by integrating a circle. Distribute integration to different
 * PEs and collect the partial sums using MPI_Reduce.
 * MPI_Reduce is measurably faster if nPE > 2
 */

#include <stdio.h>
#include <mpi.h>

const int n = 10000000;
int me, npe, tag=8;
MPI_Status status;

double calc_integral( const double start, const double step, const int steps );
double f( const double x );

int main() {
  MPI_Init( 0, NULL );
  MPI_Comm_size( MPI_COMM_WORLD, &npe );
  MPI_Comm_rank( MPI_COMM_WORLD, &me );

  double sum, buf, t0;
  double start = (me==0) ? 0.0 : 1.0*me/npe;

  //==========================================================================
  // send/receive
  //==========================================================================
  {
    if (me == 0) printf( "Calculating integral with MPI_Recv\n" );
    sum = calc_integral( start, 1.0/n, n/npe );
    if (me == 0) {
      t0 = MPI_Wtime();
      for( int pe=1; pe < npe; pe++ ) {
	MPI_Recv( &buf, 1, MPI_DOUBLE, pe, tag, MPI_COMM_WORLD, &status );
	sum += buf;
      }
      
      printf( "    Result: %f\n    %fs\n\n", sum, MPI_Wtime() - t0 );
    }
    else {
      MPI_Send( &sum, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD );
    }
  }
  //==========================================================================
  // reduce
  //==========================================================================
  {
    if (me == 0) printf( "Calculating integral with MPI_Reduce\n" );
    sum = calc_integral( start, 1.0/n, n/npe );
    t0 = MPI_Wtime();    
    MPI_Reduce( &sum, &buf, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
    if (me == 0 ) printf( "    Result: %f\n    %fs\n\n", buf, MPI_Wtime() - t0 );
  }
  
  MPI_Finalize();

  return 0;
}

inline double f( const double x ) {
  return  4.0/(1.0+x*x);
}

// trapez-rule
double calc_integral( const double start, const double step, const int steps ) {
  double sum = 0.0;
  double x = start;
  for( int i=0; i < steps; i++ ) {
    sum += ( f(x) + f(x+step) ) * 0.5 * step;
    x += step;
  }
  return sum;
}
