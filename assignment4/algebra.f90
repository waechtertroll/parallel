program algebrastuff
  use omp_lib
  implicit none

  integer, parameter            :: rk = selected_real_kind( 12 ), MAXEXP = 6
  real(kind=rk),allocatable     :: vA(:), vB(:), vC(:)
  real(kind=rk),allocatable     :: mA(:,:), mB(:,:), mC(:,:)
  integer                       :: i, DIM = 10
  real                          :: tmp

  ! numerical tests
  allocate( vA(4), vB(4), vC(4) )
  call random_number( vA )
  allocate( mA(2,2), mB(2,2), mC(2,2) )
  mA = reshape( (/ 1, 2, 3, 4 /), (/ 2, 2 /) )
  mB = reshape( (/ 1, 0, 0, 1 /), (/ 2, 2 /) )

  ! test addition
  vB = -vA
  if ( size( vA ) <= 4 ) then
     print *, "b = -a"
     tmp = addVecsAB( vA, vB, vC )
     print *, "a+b:", vC
  else
     do i=1, size(vA)
        if ( abs( vC(i) ) > 1e-12 ) then
           print *, "numerical error"
           stop 1
        end if
     end do
     print *, "a+b = 0"
  end if
  print *

  ! test addition of scalar product
  vB = -2*vA
  if ( size( vA ) <= 4 ) then
     print *, "b = -a"
     tmp = addVecsSApB( 2.0_rk, vA, vB, vC )
     print *, "a+b:", vC
  else
     do i=1, size(vA)
        if ( abs( vC(i) ) > 1e-12 ) then
           print *, "numerical error"
           stop 1
        end if
     end do
     print *, "a+b = 0"
  end if
  print *

  ! test matrix multiplication
  print *, "A:", mA
  tmp = mulMats( mA, mB, mC )
  print *, "A*E", mC

  ! now for speed tests...
  deallocate( vA, vB, vC, mA, mB, mC )
  open( unit=11, file="statistics.csv" )
  open( unit=12, file="efficiency.csv" )
  do i=1, MAXEXP
     print *, "dimension:", DIM
     allocate( vA(DIM), vB(DIM), vC(DIM), mA(DIM,DIM), mB(DIM,DIM), mC(DIM,DIM) )
     call random_number( vA )
     call random_number( vB )
     call random_number( mA )
     call random_number( mB )
     write (unit=11,fmt="(i8)",advance="no") DIM
     write (unit=12,fmt="(i8)",advance="no") DIM

     print *, "Adding vectors"
     call writeStatFiles( addVecsAB( vA, vB, vC ), addVecsABserial( vA, vB, vC ) )

     print *, "Adding vectors and scalar product"
     call writeStatFiles( addVecsSApB( 1.0_rk, vA, vB, vC ), addVecsSApBserial( 1.0_rk, vA, vB, vC ) )
     print *, "multiplying matrices"
     call writeStatFiles( mulMats( mA, mB, mC ), mulMatsSerial( mA, mB, mC ) )
     print *, "done."
     write(unit=11,fmt="()")
     write(unit=12,fmt="()")
     deallocate( vA, vB, vC, mA, mB, mC )     
     DIM = DIM * 2
  end do
  close(unit=11)

contains
  ! prints efficiency to unit12 and times to unit11
  subroutine writeStatFiles( parTime, serTime )
    real :: parTime, serTime
    write( unit=11, fmt="(t10,e8.3,t20,e8.3)", advance="no" ) parTime, serTime
    write( unit=12, fmt="(t10,e8.3)", advance="no" ) serTime/parTime
  end subroutine writeStatFiles
    

  ! sets c to a + b
  real function addVecsAB( a, b, c )
    real(kind=rk),intent(in)   :: a(:), b(:)
    real(kind=rk),intent(out)  :: c(:)
    integer                    :: i
    real                       :: t

    if ( size(a) /= size(b) ) then
       print *, "size(a) /= size(b)"
       stop 1
    end if
    
    !$omp parallel default(none) shared(a,b,c) reduction(max:t)
    t = omp_get_wtime()
    !$omp parallel do
    do i=1,size(a)
       c(i) = a(i) + b(i)
    end do
    !$omp end parallel do
    t = omp_get_wtime()-t
    !$omp end parallel
    addVecsAB = t
    print *, "parallel", t
  end function addVecsAB

! sets c to a + b
  real function addVecsABserial( a, b, c )
    real(kind=rk),intent(in)   :: a(:), b(:)
    real(kind=rk),intent(out)  :: c(:)
    integer                    :: i
    real                       :: t1, t2

    if ( size(a) /= size(b) ) then
       print *, "size(a) /= size(b)"
       stop 1
    end if
    
    call cpu_time(t1)
    do i=1,size(a)
       c(i) = a(i) + b(i)
    end do
    call cpu_time(t2)
    print *, "serial", t2-t2
    addVecsABserial = t2-t1
  end function addVecsABserial

  ! sets c to s*a + b
  real function addVecsSApB( s, a, b, c )
    real(kind=rk),intent(in)  :: s, a(:), b(:)
    real(kind=rk),intent(out) :: c(:)
    integer                   :: i
    real                      :: t

    if ( size(a) /= size(b) ) then
       print *, "size(a) /= size(b)"
       stop 1
    end if
    
    !$omp parallel default(none) shared(s,a,b,c) reduction(max:t)
    t = omp_get_wtime()
    !$omp parallel do
    do i=1,size(a)
       c(i) = s*a(i) + b(i)
    end do
    !$omp end parallel do
    t = omp_get_wtime()-t
    print *, "parallel", t
    !$omp end parallel
    addVecsSApB = t
  end function addVecsSApB


  ! sets c to s*a + b
  real function addVecsSApBSerial( s, a, b, c )
    real(kind=rk),intent(in)  :: s, a(:), b(:)
    real(kind=rk),intent(out) :: c(:)
    integer                   :: i
    real                      :: t1, t2

    if ( size(a) /= size(b) ) then
       print *, "size(a) /= size(b)"
       stop 1
    end if
    
    call cpu_time(t1)
    do i=1,size(a)
       c(i) = s*a(i) + b(i)
    end do
    call cpu_time(t2)
    print *, "serial:", t2-t1
    addVecsSApBserial = t2-t2
  end function addVecsSApBSerial

  ! sets C to A * B
  real function mulMats( A, B, C )
    real(kind=rk),intent(in)  :: A(:,:), B(:,:)
    real(kind=rk),intent(out) :: C(:,:)
    real(kind=rk)             :: tmpSum
    integer                   :: i, j, k
    real                      :: t

    !print *, size( A(:,1) ), size( A(1,:) )
                  ! row             col

    if ( size( A(:,1) ) /= size( B(1,:) ) ) then
       print *, "Error: rows(A) /= cols(B)"
       stop 1
    end if

    C=0
    !$omp parallel default(none) shared(A,B,C) reduction(max:t) if( size(A(:,1)) >= 300 )
    t = omp_get_wtime()
    !$omp parallel do if( size(A(:,1)) >= 300 )
    do i=1, size( A(:,1) )
       do j=1, size( B(1,:) )
          do k=1, size( A(1,:) )
             C(i,j) = C(i,j) + A(k,i) * B(j,k) ! invert indices beause of fortran
          end do
       end do
    end do
    !$omp end parallel do
    t = omp_get_wtime()-t
    print *, "parallel", t
    !$omp end parallel
    mulMats = t
  end function mulMats

! sets C to A * B
  real function mulMatsSerial( A, B, C )
    real(kind=rk),intent(in)  :: A(:,:), B(:,:)
    real(kind=rk),intent(out) :: C(:,:)
    real(kind=rk)             :: tmpSum
    integer                   :: i, j, k
    real                      :: t1, t2

    !print *, size( A(:,1) ), size( A(1,:) )
                  ! row             col

    if ( size( A(:,1) ) /= size( B(1,:) ) ) then
       print *, "Error: rows(A) /= cols(B)"
       stop 1
    end if

    C=0
    call cpu_time(t1)
    do i=1, size( A(:,1) )
       do j=1, size( B(1,:) )
          do k=1, size( A(1,:) )
             C(i,j) = C(i,j) + A(k,i) * B(j,k) ! invert indices beause of fortran
          end do
       end do
    end do
    call cpu_time(t2)
    print *, "serial:", t2-t1
    mulMatsSerial = t2-t1
  end function mulMatsSerial

end program algebrastuff
