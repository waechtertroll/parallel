program warmup
  use omp_lib
  implicit none

  print *, "Serial"
  call print_omp_stats()
  print *

  print *, "Parallel I (std)"
  call parallel_printing()

  print *, "Parallel II (num_procs)"
  call omp_set_num_threads( omp_get_num_procs() )
  call parallel_printing()

  print *, "Parallel III(num_procs+5)"
  call omp_set_num_threads( omp_get_num_procs() + 5 )
  call parallel_printing

contains
  subroutine print_omp_stats()
    print *, "  threads:", omp_get_num_threads(), "this:", omp_get_thread_num(), "procs:", &
         &omp_get_num_procs(), "parallel?", omp_in_parallel()
  end subroutine print_omp_stats

  subroutine parallel_printing()
    !$omp parallel
    call print_omp_stats()
    !$omp end parallel
    print *
  end subroutine parallel_printing
    

end program warmup
