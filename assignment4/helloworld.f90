program helloParallelWorld
  use omp_lib
  implicit none
  integer :: n
  
  ! gfortran -fopenmp -o helloworld helloworld.f90
  ! OMP_NUM_THREADS=x ./helloworld
  !$omp parallel
  print *, "Hello world"
  !$omp end parallel

  write (*,"(a)",advance='no'), "Number of threads?"
  read *, n
  call omp_set_num_threads( n )
  !$omp parallel
  print *, "Hello world"
  !$omp end parallel
end program helloParallelWorld
