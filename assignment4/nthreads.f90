program algebrastuff
  use omp_lib
  implicit none

  integer, parameter            :: rk = selected_real_kind( 18 ), MAXEXP = 5
  real(kind=rk),allocatable     :: mA(:,:), mB(:,:), mC(:,:)
  integer                       :: i, DIM = 10000
  real                          :: tomp, tsingle

  allocate( mA(dim,dim), mB(dim,dim), mC(dim,dim) )
  call random_number( mA )
  call random_number( mB )

  open( unit=11, file="eff_tnums.csv" )
  tsingle = mulMatsSerial( mA, mB, mC )
  
  do i=1, 25
     call omp_set_num_threads( i )
     tomp = mulMats( mA, mB, mC )
     write ( unit=11, fmt="(i8,t10,e8.3)" ), &
       & i, tsingle / tomp
  end do
  close( unit=11 )

contains
  ! sets C to A * B
  real function mulMats( A, B, C )
    real(kind=rk),intent(in)  :: A(:,:), B(:,:)
    real(kind=rk),intent(out) :: C(:,:)
    real(kind=rk)             :: tmpSum
    integer                   :: i, j, k
    real                      :: t

    !print *, size( A(:,1) ), size( A(1,:) )
                  ! row             col

    if ( size( A(:,1) ) /= size( B(1,:) ) ) then
       print *, "Error: rows(A) /= cols(B)"
       stop 1
    end if

    C=0
    !$omp parallel default(none) shared(A,B,C) reduction(max:t)
    t = omp_get_wtime()
    !$omp parallel do
    do i=1, size( A(:,1) )
       do j=1, size( B(1,:) )
          do k=1, size( A(1,:) )
             C(j,i) = C(j,i) + A(k,i) * B(j,k) ! invert indices beause of fortran
          end do
       end do
    end do
    !$omp end parallel do
    t = omp_get_wtime()-t
    !$omp end parallel
    print *, "parallel: ", t
    mulMats = t
  end function mulMats

! sets C to A * B
  real function mulMatsSerial( A, B, C )
    real(kind=rk),intent(in)  :: A(:,:), B(:,:)
    real(kind=rk),intent(out) :: C(:,:)
    real(kind=rk)             :: tmpSum
    integer                   :: i, j, k
    real                      :: t1, t2

    !print *, size( A(:,1) ), size( A(1,:) )
                  ! row             col

    if ( size( A(:,1) ) /= size( B(1,:) ) ) then
       print *, "Error: rows(A) /= cols(B)"
       stop 1
    end if

    C=0
    call cpu_time(t1)
    do i=1, size( A(:,1) )
       do j=1, size( B(1,:) )
          do k=1, size( A(1,:) )
             C(j,i) = C(j,i) + A(k,i) * B(j,k) ! invert indices beause of fortran
          end do
       end do
    end do
    call cpu_time(t2)
    print *, "serial:", t2-t1
    mulMatsSerial = t2-t1
  end function mulMatsSerial

end program algebrastuff
