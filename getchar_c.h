#ifndef GETCHAR_C_H
#define GETCHAR_C_H

#include <stdio.h>
#include <unistd.h>  // fuer STDIN_FILENO
#include <termios.h> // fuer tc....

char GetChar() {
    struct termios oldterm;
    tcgetattr(STDIN_FILENO, &oldterm);
    struct termios newterm = oldterm;
    newterm.c_lflag = newterm.c_lflag & ~ICANON;
    newterm.c_lflag = newterm.c_lflag & ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &newterm);
    char c = '\0';
    read(STDIN_FILENO, &c, 1);
    tcsetattr(STDIN_FILENO, TCSANOW, &oldterm);
    return c;
}

#endif
