! Calculates the Poisson equation for a LxL 2-dimensional problem on a rectangular
! grid of discrete values with equal distances.
! Distributes the problem between machines using MPI and parallelizes work on
! individual machines using OpenMP.
! Note that the number of processors used must make up the area of a rectangle with
! integer side lengths.

PROGRAM Poisson
  use omp_lib
  use mpi
  implicit none

  integer,parameter       :: L=9, MAXITER=3
  real,parameter          :: c=1
  real,parameter          :: epsilon=1e-8
  real,allocatable        :: src(:,:)
  real,allocatable,target :: phi(:,:)

  ! ori is index of upper left array element in complete rectangle
  integer           :: me, npe, io, rows, cols, ori(2)
  integer           :: left, right, up, down, xProcs, yProcs

  integer           :: MPI_ROW, MPI_COL

  call setupNodes()
  allocate( phi(0:cols+1, 0:rows+1), src(1:cols, 1:rows) )

  call setSource()
  call dirichletBoundaryCondition()

  call solve()
!  if (me == 0) call printData()

  call exchangeBorders()

! DEBUG
!  print *, me+1, size( phi(1,:) ), "cols", size( phi(:,1) ), "rows"
!  do i=0, rows+1
!     print *, (me+1)*100+i, phi(i,:)
!  end do
!  print *, (me+1)*100

  deallocate( phi )
  deallocate( src )
  call MPI_Finalize( io )

contains
  ! =================================================================================
  ! calculate node position, field size, field position in the original rectangle,
  ! neighbours, and create MPI datatypes for rows and columns
  ! =================================================================================
  SUBROUTINE setupNodes()
    integer :: xPos, yPos ! positions of this PE in the virtual rectangle
    
    call MPI_Init( io )
    call MPI_Comm_size( MPI_COMM_WORLD, npe, io )
    call MPI_Comm_rank( MPI_COMM_WORLD, me, io )

    ! try to distribute procs on a rectangular grid
    if (npe ==1) then
       rows = L
       cols = L
       ori = 0
       left = 0
       right = 0
       up = 0
       down = 0
    elseif (npe > L*L) then
       if (me == 0) print *, "Should really use less nodes than matrix elements!"
       call MPI_Finalize( io )
       stop
    else
       yProcs = -1
       ! test if npe is the area of a rectangle with integer sides
       ! by division with numbers between (sqrt(npe)+1) and 2
       search: do xProcs=int( sqrt( npe*1.0 ) )+1, 2, -1
          if ( mod( npe, xProcs ) == 0 ) then
             yProcs = npe/xProcs
             exit search
          end if
       end do search
       ! test if appropriate side lengts could be found
       if (yProcs > -1) then
          xPos = mod( me, xProcs )
          yPos = me / xProcs
       ! else exit with error
       else
          if (me ==0) print *, "nPE must form an integer rectangle?!"
          call MPI_Finalize( io )
          stop
       end if
       ! now split the data field evenly
       rows = L / yProcs
       cols = L / xProcs
       ori(1) = xPos * cols + 1
       ori(2) = yPos * rows + 1
       ! make sure nothing is omitted
       if (xPos == xProcs-1) cols = L - ori(1) + 1
       if (yPos == yProcs-1) rows = L - ori(2) + 1
! DEBUG STATEMENT
!       print *, me, "xPos:", xPos, "yPos:", yPos, "Data:", ori
       ! calculate neighbours
       if (xPos == 0) then
          left = me + xProcs - 1
       else
          left = me - 1
       end if

       if (xPos == xProcs-1) then
          right = me - xProcs + 1
       else
          right = me + 1
       end if

       if (yPos == 0) then
          up = npe - (xProcs - xPos)
       else
          up = me - xProcs
       end if

       if (yPos == yProcs-1) then
          down = xPos
       else
          down = me + xProcs
       end if
! DEBUG STATEMENT
!       print *, me, left, "left", right, "right", up, "up", down, "down"
    end if

    call MPI_Type_contiguous( rows, MPI_REAL, MPI_COL, io )
    call MPI_Type_vector( cols, 1, rows+2, MPI_REAL, MPI_ROW, io )
    call MPI_Type_Commit( MPI_COL, io )
    call MPI_Type_Commit( MPI_ROW, io )
  end subroutine setupNodes

  ! =================================================================================
  ! exchange copies of each local field's borders and store them as shadow copies
  ! =================================================================================
  SUBROUTINE exchangeBorders()
    real     :: buf(max(cols,rows))
    integer  :: tag=42

    ! send right active column to right node's left
    call MPI_Sendrecv( &
         & phi(1,1), 1, MPI_COL, right, tag, &
         & buf, 1, MPI_COL, left, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, io )
    phi(1:rows,0) = buf
    ! send left active column to left node's right
    call MPI_Sendrecv( &
         & phi(1,cols), 1, MPI_COL, left, tag, &
         & buf, 1, MPI_COL, right, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, io )
    phi(1:rows,cols+1) = buf
    ! send top active row to upper node's bottom
    ! mind the different datatyes, transforming indexed vector to dense one
    call MPI_Sendrecv( &
         & phi(1,1), 1, MPI_ROW, up, tag, &
         & buf, 1, MPI_COL, down, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, io )
    phi(rows+1,1:cols) = buf
    ! send bottom active row to lower node's top
    call MPI_Sendrecv( &
         & phi(rows,1), 1, MPI_ROW, down, tag, &
         & buf, 1, MPI_COL, up, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, io )
    phi(0,1:cols) = buf
    
  end subroutine exchangeBorders

  ! =================================================================================
  ! applies Jacobi iteration until convergence to epsilon or maximum number
  ! of iterations is reached
  ! =================================================================================
  SUBROUTINE solve()
    logical  :: converged
    real     :: phi_old
    integer  :: i, j, iter, eo
    
    iter = 0
    converged = .false.

    do while( .not. converged .and. iter <= MAXITER )
       iter = iter + 1
       if (me == 0) print *, "# iteration #", iter
       converged = .true.
       ! checkerboard partition
       do eo=0, 1
          ! work on (even,even) / (odd,odd)
          !$omp parallel do default(none) reduction(.and.:converged) private(phi_old) shared(src,phi,eo,rows,cols)
          do i=(1+eo), (cols), 2
             do j=(1+eo), (rows), 2
                phi_old = phi(i,j)
                phi(i,j) = ( &
                     phi(i+1,j) + phi(i-1,j) + &
                     phi(i,j+1) + phi(i,j-1) - &
                     src(i,j) ) * 0.25
                if ( abs( (phi(i,j) - phi_old) / phi(i,j) ) > epsilon ) converged = .false.
             end do
          end do
       end do
       do eo=0, 1
          ! work on (even,odd) / (odd,even)
          !$omp parallel do default(none) reduction(.and.:converged) private(phi_old) shared(src,phi,eo,rows,cols)
          do i=(1 + mod( eo+1, 2 ) ), (cols), 2
             do j=(1+eo), (rows), 2
                phi_old = phi(i,j)
                phi(i,j) = ( &
                     phi(i+1,j) + phi(i-1,j) + &
                     phi(i,j+1) + phi(i,j-1) - &
                     src(i,j) ) * 0.25
                if ( abs( (phi(i,j) - phi_old) / phi(i,j) ) > epsilon ) converged = .false.
             end do
          end do
       end do
       call dirichletBoundaryCondition()
       call exchangeBorders()
    end do
    if (me == 0) print *, "# Iterations needed:", iter
  end subroutine solve

  ! extend to accept user defined function if time left
  ! =================================================================================
  ! store source function values for local partition of the problem
  ! =================================================================================
  SUBROUTINE setSource()
    integer :: x, y
    !$omp parallel do default(none) shared(src) collapse(2) shared(ori,me)
    do x=1, cols
       do y = 1, rows
! DEBUG statement
!          print *, me, "setting", x, y, "from", ori(1)+x-1, ori(2)+y-1
          src(x,y) = f( ori(1)+x-1, ori(2)+y-1 )
       end do
    end do
  end subroutine setSource

  ! =================================================================================
  ! apply boundary condition on local partition of phi field
  ! =================================================================================
  SUBROUTINE dirichletBoundaryCondition()
    if (ori(1) == 1) then           ! if field contains leftmost column
       phi( 1,1:rows ) = 0          ! set it to 0
    elseif (ori(1)+cols-1 == L) then  ! if field contains rightmost column
       phi(cols,1:rows) = 0         ! set it to 0
    end if

    if (ori(2) == 1) then           ! if field contains first row
       phi(1:cols,1) = 0            ! set it to 0
    elseif (ori(2)+rows-1 == L) then  ! if field contains last row
       phi(1:cols,rows) = 0         ! set it to 0
    end if
  end subroutine dirichletBoundaryCondition

  ! =================================================================================
  ! extend to read filename
  ! print data to file "testplot.gnu" in format
  ! x, y, z; still need good idea to plot nicely
  ! currently: 
  ! gnuplot: set dgrid3d 20,20,2
  !          splot "testplot.gnu"
  ! =================================================================================
!  SUBROUTINE printData()
!    integer :: x, y
!    open( unit=11, file="testplot.gnu", status="replace" )
!    do  x=1, L
!       do y=1, L
!          write (unit=11,fmt="(I9,t12,I9,t24,e20.10)"), x, y, phi(x,y)
!       enddo
!    enddo
!    close( unit=11 )
  !  end subroutine printData

  SUBROUTINE printData()
    integer      :: i, j, outfile, offset
    real         :: xVals(cols), yVals(rows)
    real,pointer :: zVals(:,:)
    ! prepare data blocks for output
    do i=1, cols
       xVals(i) = ori(1) + i - 1
    end do
    do j=1, rows
       yVals(i) = ori(2) + j - 1
    end do
    zVals => phi(1:cols,1:rows)
          
    call MPI_File_open( MPI_COMM_WORLD, "testplot.gnu", &
         & MPI_MODE_WRONLY+MPI_MODE_CREATE, MPI_INFO_NULL, outfile, io )

    call MPI_File_close( outfile, io )
    
  end subroutine printData

  ! =================================================================================
  ! return function value for given x and y position
  ! =================================================================================
  real FUNCTION f( x, y )
    integer,intent(in) :: x, y
    real,parameter     :: onebyl = 1.0/L
    f = onebyl * exp( -c * ( (x * onebyl - 0.5)**2 + (y * onebyl - 0.5)**2 ) )
  end function f

end program Poisson
