! Exercise 9-2: solve 2-dimensional Poisson equation.
! Non-square number of processors not implemented due to time reasons.
! Distributes problem over nodes using MPI, calculates on available cores using
! OpenMP if compiled with -fopenmp.
! C-Prepocessor compile params:
!  -DITER=x       integer, maximum number of iterations
!  -DGRIDSIZE=x   integer, grid points per node. Be sure to erase *.gnu before 
!                 running program after changing gridsize.
!  -DEPSILON=x    real, epsilon used for convergence
! Plot result files with gnuplot:
!  set hidden3d (optional)
!  splot <file> binary w l

program Poisson
  use mpi
  implicit none

#ifdef ITER
  integer,parameter     :: IT_MAX=ITER
#else 
  integer,parameter     :: IT_MAX=10000
#endif

#ifdef GRIDSIZE
  integer,parameter     :: N=GRIDSIZE
#else
  integer,parameter     :: N=11
#endif

#ifdef EPSILON
  real,parameter        :: eps=EPSILON
#else
  real,parameter        :: eps=1e-5
#endif

  real,parameter        :: c=5.0
  real                  :: h
  real,allocatable      :: src(:,:), phi(:,:), buf(:)
  integer               :: L, i, j, iter=0

  integer               :: me, procs, xPos, yPos, ie, xProcs, yProcs
  real                  :: oriX, oriY
  integer               :: MPI_ROW, MPI_COL, MPI_SRC_ROW
  integer               :: left, right, up, down

  call initNode()
  call initSource()
  phi = 0
  phi(:,1) = me
  phi(:,2) = me*10
  phi(:,N) = me
  call solve()
  call writeOutput( "source.gnu", src, MPI_SRC_ROW )
  call writeOutput( "solution.gnu", phi, MPI_ROW )
  
  call cleanExit()

contains

!===============================================================================
! Initialise this node's fraction of the source function
!===============================================================================
  SUBROUTINE initSource()
    real    :: fac, x, y
    fac = 1.0 / L
    allocate( src(N,N), phi(0:N+1,0:N+1) )
    allocate( buf(max( N, N )) )
    !$omp parallel do collapse(2) shared(oriX,oriY,src) private(x,y)
    do j=1, N
       do i=1, N
          x = i+oriX
          y = j+oriY
          src(j,i) = fac * exp( -c * ((x*fac-0.5)**2 + (y*fac-0.5)**2 ) )
       end do
    end do
  end subroutine initSource

!===============================================================================
! Apply dirichlet boundary condition exploiting the fact that phi is initialised
! as 0 by setting start- and end values for solve()'s loop indices so 
! phi stays 0 for all x,y in boundary.
!===============================================================================  

  SUBROUTINE dirichletBoundary( vec )
    integer :: vec(4)
    vec = (/ 1, N, 1, N /)
    if (xPos == 0) vec(1) = 2
    if (xPos == xProcs-1) vec(2) = N-1
    if (yPos == 0) vec(3) = 2
    if (yPos == yProcs-1) vec(4) = N-1
  end subroutine dirichletBoundary

!===============================================================================
! Solve by applying Jacobi's iteration on a checkerboard pattern, echange
! boundaries between nodes after every switch from white to black and vice versa
!===============================================================================
  
  SUBROUTINE solve()
    real    :: oldphi
    real    :: dev, gdev ! deviance, global deviance
    integer :: eo, oe
    integer :: limits(4) ! array of start- and end positions for i and j on this node
    call dirichletBoundary(limits)
    print *, me, "has limits:", limits
    phi = 0.0
    gdev = 1.0
    do while (gdev >= eps .and. iter < IT_MAX)
       dev = 0.0
       iter = iter + 1
       do oe=0,1
          do eo=0,1
             !$omp parallel do collapse(2) shared(eo,oe,phi,src) private(oldphi) reduction(max:dev)
             do i=limits(3)+eo, limits(4), 2
                do j=limits(1)+mod( eo+oe, 2 ), limits(2), 2

                   oldphi = phi(i,j)
                   phi(i,j) = ( phi(i+1,j) + phi(i-1,j) + phi(i,j+1) + phi(i,j-1) - src(i,j) ) * 0.25
                   dev = max( abs( oldphi - phi(i,j) ), dev )
                end do
             end do
          end do
          call exchangeBorders()
       end do
       call MPI_Allreduce( dev, gdev, 1, MPI_REAL, MPI_MAX, MPI_COMM_WORLD, ie )
       if (me == 0) print *, "Iteration:", iter, "deviation:", gdev
    end do
    if (me == 0) then
       if (gdev < eps) then
          print *, "Converged after", iter, "iterations"
       else
          print *, "Did not converge yet. Try recompiling with bigger ITER or plotting output file."
       end if
    end if
    
  end subroutine solve

!===============================================================================
! Write a file of 3d coordinates in gnuplot binary format by using
! MPI_File_write_ordered. Different rowtypes are needed for source function
! and phi because of different dimensions.
!===============================================================================
  
  SUBROUTINE writeOutput( filename, field, rowtype )
    integer,intent(in) :: rowtype
    real,intent(in)    :: field(:,:)
    integer            :: startProc, endProc, newline=10, i, file, linestart
    real,allocatable   :: x(:)
    real               :: y
    character(*)       :: filename

    linestart = 1
    if (rowtype == MPI_ROW) linestart = 2

    call MPI_File_open( MPI_COMM_WORLD, filename, &
         & MPI_MODE_WRONLY+MPI_MODE_CREATE, MPI_INFO_NULL, file, ie )
    if (me == 0) then  ! Cols+1, x values, endl, y_1, row_1, endl, y_2,...
       print *, "Writing ", filename, "..."
       allocate( x(0:N*xProcs) )
       x(0) = real(N * xProcs) + 1.0
       do i=1, N*xProcs
          x(i) = real(i-1)
       end do
       call MPI_File_write_shared( file, x, size( x ), MPI_REAL, &
            MPI_STATUS_IGNORE, ie )
       call MPI_File_write_shared( file, newline, 1, MPI_INTEGER, &
            MPI_STATUS_IGNORE, ie )
    end if

    startProc = -xProcs
    do i=1, N*xProcs
       if (mod( i-1, N ) == 0 ) startProc = startProc + xProcs
       endProc = startProc + xProcs - 1
       y = 2.0 * real(i-1)
       if (me == 0) call MPI_File_write_shared( file, y, 1, MPI_REAL, &
            MPI_STATUS_IGNORE, ie )
       if (me >= startProc .and. me <= endProc) then
          call MPI_File_write_ordered( file, field(mod( i-1, N )+1,linestart), 1, rowtype, &
               MPI_STATUS_IGNORE, ie )
       else ! all write in order nothing just for write_ordered to finish
          call MPI_FILE_write_ordered( file, 0, 0, MPI_REAL, &
               MPI_STATUS_IGNORE, ie )
       end if
       if (me == 0) call MPI_File_write_shared( file, newline, 1, MPI_INTEGER, &
            MPI_STATUS_IGNORE, ie )
    end do

    call MPI_File_close( file, ie )
    
  end subroutine writeOutput

!===============================================================================
! Calculates values for this node's virtual grid position, neighbours,
! MPI datatypes for later row/column exchange, inits MPI.
! Should be called first.
!===============================================================================

  SUBROUTINE initNode()
    call MPI_Init( ie )
    call MPI_Comm_size( MPI_COMM_WORLD, procs, ie )
    call MPI_Comm_rank( MPI_COMM_WORLD, me, ie )

    xProcs = int(sqrt( real(procs) ))
    yProcs = xProcs

    if (xProcs*xProcs /= procs) then
       if (me == 0) print *, "Please use a square number of nodes."
       call cleanExit()
       stop 1
    end if

    yPos = me / xProcs
    xPos = mod( me, xProcs )

    if (xPos == 0) then
       left = me + xProcs - 1
    else
       left = me - 1
    end if

    if (xPos == xProcs - 1) then
       right = me - xProcs + 1
    else
       right = me + 1
    end if

    if (yPos == 0) then
       up = procs - xProcs + me
    else
       up = me - xProcs
    end if

    if (yPos == yProcs - 1) then
       down = mod( me, xProcs )
    else
       down = me + xProcs
    end if

    L = xProcs * N
    h = L / xProcs / N
    oriX = xPos * N
    oriY = yPos * N

    call MPI_Type_contiguous( N, MPI_REAL, MPI_COL, ie )
    call MPI_Type_vector( N, 1, N+2, MPI_REAL, MPI_ROW, ie )
    call MPI_Type_vector( N, 1, N, MPI_REAL, MPI_SRC_ROW, ie )
    call MPI_Type_commit( MPI_COL, ie )
    call MPI_Type_commit( MPI_ROW, ie )
    call MPI_Type_commit( MPI_SRC_ROW, ie )

  end subroutine initNode

!===============================================================================
! Exchanges borders with neighbours and stores them as shadow copies.
!===============================================================================

  SUBROUTINE exchangeBorders()
    integer :: tag=42

    call MPI_Sendrecv( phi(1,1), 1, MPI_COL, left, tag,  &
         & phi(1,N+1), 1, MPI_COL, right, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, ie )
    call MPI_Sendrecv( phi(1,N), 1, MPI_COL, right, tag, &
         & phi(1,0), 1, MPI_COL, left, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, ie )
    call MPI_Sendrecv( phi(1,1), 1, MPI_ROW, up, tag, &
         & phi(N+1,1), 1, MPI_ROW, down, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, ie )
    call MPI_Sendrecv( phi(N,1), 1, MPI_ROW, down, tag, &
         & phi(0,1), 1, MPI_ROW, up, tag, &
         & MPI_COMM_WORLD, MPI_STATUS_IGNORE, ie )
  end subroutine exchangeBorders

  SUBROUTINE cleanExit()
    if (allocated( phi ) ) deallocate( phi )
    if (allocated( src ) ) deallocate( src )
    if (allocated( buf ) ) deallocate( buf )
    call MPI_Finalize( ie )
  end subroutine cleanExit
  
end program Poisson
