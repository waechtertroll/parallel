function force(r, i) result (F)
   implicit none
   real, parameter                     :: pi2 = 1.5707963

   real, allocatable, intent(in)       :: r(:,:)   !coords of all particles
   integer, intent (in)                :: i        !particle number
   integer                             :: N        !total particles
   integer                             :: DIMS     !Dimensions
   real, allocatable                   :: F(:)     !Force

   real,allocatable                    :: rij(:)   !distance of 2 particles
   real                                :: rij_norm2
   real                                :: rn       
   integer                             :: j        !iterator

   N = size(r(1,:))
   DIMS = size(r(:,1))
   allocate(rij(N),F(DIMS))

   do j=1, N
      F = 0
      if (i /= j) then
         rij = r(:,i) - r(:,j)
         rij_norm2 = norm2(rij)
         rn = min(rij_norm2, pi2)
         F = F  - rij / norm2(rij) * 2 *sin(rn)*cos(rn)
      end if
   end do

   deallocate(rij)
end function force
 


program Aufg3

implicit none


!------------ physical system variables ---------
integer, parameter               :: DIMS = 2       !dimensions
integer, parameter               :: L    = 1       !length of system
real                             :: t    = 0       !time
real                             :: dt             !timestep
real, parameter                  :: dt_o = 0.001   !orig. timestep
real                             :: Epot           !potential energy
real                             :: Ekin           !kinetic energy
real                             :: E              !total energy


!----------    particles  variables -----------
integer, parameter               :: N    = 50      !number of particles
real, allocatable                :: r(:, :)        !coordinates
real, allocatable                :: v(:, :)        !velocities
real, allocatable                :: a(:, :)        !accelerations
real, allocatable                :: a_copy(:)
real                             :: m = 1          !masses

!----------   simluation variables -----------
integer                          :: i,j,k          !iterators
real, parameter                  :: MAX_ERR=0.5    !maximum error in percent
                                                   !for energy
integer                          :: seed_var=10    !seed for random numbers

interface
   function force(r,i) result(F)
      real, allocatable, intent(in)       :: r(:,:)
      integer, intent (in)                :: i
      real, allocatable                   :: F(:)
   end function force
end interface


!allocate memory
allocate(r(DIMS,N), v(DIMS,N), a(DIMS,N), a_copy(DIMS))

!fill with random numbers
call random_seed(seed_var)
call random_number(r)
call random_number(v)
call random_number(a)


!calculate energy of system
Ekin = 0
Epot = 0
do i=1,N
   do j=1, N
      if( i /= j) then
         Epot = Epot + 0.5*sin(norm2(r(:,i)-r(:,j)))**2
      end if
   end do
   Ekin = Ekin + 0.5 * m * dot_product(v(:,i), v(:, i))   
end do
E = Epot + Ekin



!file for output
open(20,file="outputVerlet.dat",status='OLD')



!velocity verlet algorithm  :     i particle, j dimension, k timestep
dt   = dt_o
do k=1, 100000
   !write coordinates to file
   write(20,*) "time = ", t
   write(20,*) "coords of one dim of all particles "
   do i=1,DIMS
      write(20,*) r(i,:)
   end do
   !write energy to file
   write(20,*) "Epot and Ekin of all partilces = "
   do i=1, DIMS
      write(20,*) Epot, Ekin
   end do


   do i=1, N
      r(:,i)     = r(:,i) + v(:,i)*dt + 0.5*a(:,i)*dt*dt
         !collision detection
         do j=1, DIMS
            if ( r(j,i) > 1 ) then 
               r(j,i) = 1 - ( r(j,i)-1 ) 
               v(j,i) = - v(j,i) 
            end if 
            if ( r(j,i) < 0 ) then 
               r(j,i) = -r(j,i) 
               v(j,i) = -v(j,i)
            end if
         end do
      a_copy(:)  = a(:,i)
      a(:, i)    = 1 / m * force(r, i)
      v(:,i)     = v(:,i) + 0.5*( a(:,i) + a_copy(:) ) * dt
   end do
   t = t + dt
 !check for energy conservation
   !Epot
   Epot = 0
   Ekin = 0
   do i=1, N
      do j=1, N
         if(i /= j) then
            Epot = Epot + 0.5*sin(norm2(r(:,i)-r(:,j)))**2
         end if
      end do
   end do
   !Ekin
   do i=1,N
      Ekin = Ekin + 0.5 * m * dot_product(v(:, i), v(:, i))
   end do
   
   if ( abs(E - (Ekin + Epot)) > (1+MAX_ERR)*E ) then
      write(*,*) k, "Error no energy conservation", & 
               abs(E - ( Ekin + Epot ) ) / E *100, "% deviation"
   end if

end do


!garbage collection
close(20)

deallocate(r,v,a,a_copy)
end program Aufg3



